Starting all apps (wordpress) insync two different windows - Wait til complete and errors disappear. 

Notes* On Mac, an error shows up for `127.0.0.2` docker however works fine in linux vm. 
Notes* On Linx VM, an screen and sh doesn't run in repo. 
Notes* Each docker compose up command needs to be run separately and can not be setup with the same name images because docker throughs an error.

```
docker-compose --file ./wordpress/docker-compose.yml up
```

```
docker-compose --file ./wordpress-two/docker-compose.yml up

```

```
docker-compose --file ./web/docker-compose.yml up
```

Closing all apps (wordpress) insync 

```
docker-compose --file ./wordpress/docker-compose.yml down
```
```
docker-compose --file ./wordpress-two/docker-compose.yml down
```

```
docker-compose --file ./web/docker-compose.yml down
```

For PhpMyAdmin IP:8080

```
root
password
```

For Ports Info:

```
http://127.0.0.1:80/     => Wordpress
http://127.0.0.1:8080/   => PhpMyAdmin
http://127.0.0.1:3306/   => DB
http://127.0.0.2:8080/   => PhpMyAdmin
http://127.0.0.2:80/     => Wordpress
http://127.0.0.2:3306/   => PhpMyAdmin
http://127.0.0.4:5002/   => PHP

```

